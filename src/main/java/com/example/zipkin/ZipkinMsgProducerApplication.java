package com.example.zipkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class ZipkinMsgProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinMsgProducerApplication.class, args);
	}

}
