package com.example.zipkin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {
	@Autowired
	JmsTemplate jmsTemplate;

	@GetMapping(value = "/sendMessage")
	public void sendMessage() {
		jmsTemplate.convertAndSend("backend", new Date());
	}

}
